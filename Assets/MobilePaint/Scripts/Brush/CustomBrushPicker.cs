// basic custom brush list, using selectiongrid

using UnityEngine;
using System.Collections;

namespace unitycoder_MobilePaint
{
	public class CustomBrushPicker : MonoBehaviour {

		public GameObject canvas;
		public bool closeAfterPick = true;

		public GameObject functionButton;

		private bool isEnabled = true;

		private int selGridInt = -1; // selected item

		void OnGUI()
		{

			if (!isEnabled) return;

			// selection grid of custom brushes
			selGridInt = GUILayout.SelectionGrid(selGridInt, canvas.GetComponent<DrawBehavior>().customBrushes, 8, GUILayout.MinHeight(64), GUILayout.MaxHeight(64));

			if (selGridInt>-1)
			{

				isEnabled = false; // hide guitexture just to show something is happening

				canvas.GetComponent<DrawBehavior>().selectedBrush = selGridInt;
				canvas.GetComponent<DrawBehavior>().ReadCurrentCustomBrush();
				selGridInt = -1;

				Invoke("DelayedToggle",0.4f);
			} // if


		} // OnGUI



		void DelayedToggle()
		{
			functionButton.GetComponent<GUITexture>().texture = canvas.GetComponent<DrawBehavior>().customBrushes[canvas.GetComponent<DrawBehavior>().selectedBrush] as Texture;
			functionButton.GetComponent<CustomBrushDialog>().ToggleModalBackground();
			isEnabled = true;
		}

	} // class
} // namespace