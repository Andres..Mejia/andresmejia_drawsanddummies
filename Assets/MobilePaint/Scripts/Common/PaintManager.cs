﻿using UnityEngine;
using System.Collections;

namespace unitycoder_MobilePaint
{

	public class PaintManager : MonoBehaviour 
	{
		public DrawBehavior mobilePaintReference;

		public static DrawBehavior mobilePaint;

		void Awake()
		{
			if (mobilePaintReference==null) Debug.Log("No se ha asignado "+transform.name,gameObject);

			mobilePaint = mobilePaintReference;

		}

	}
}