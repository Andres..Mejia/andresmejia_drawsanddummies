﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteTest : MonoBehaviour
{
    [SerializeField]
    private GameObject parent;

    private void Awake()
    {
        parent = GameObject.Find("Box");
    }

    void Update()
    {
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            var child = parent.transform.GetChild(i).gameObject;

            if (parent.transform.childCount == 0)
            {
                parent.transform.gameObject.SetActive(false);
            }
            else
            {
                parent.transform.gameObject.SetActive(true);
            }
        }
    }
}
