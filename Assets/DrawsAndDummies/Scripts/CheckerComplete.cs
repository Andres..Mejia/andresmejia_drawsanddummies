﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckerComplete : MonoBehaviour
{
    CheckerCollider checker;

    [SerializeField]
    private GameObject[] items;
    private int activateNext = 0;

    void Awake()
    {
        checker = GetComponent<CheckerCollider>();
    }

    void Update()
    {
        ItemStart();

        if (items == null && activateNext < items.Length)
        {
            items[activateNext].SetActive(true);

            activateNext++;
        }
    }

    void ItemActivation()
    {
        foreach(GameObject item in items)
        {
            Debug.Log("Se activa " + item);
        }
    }

    void ItemStart()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].SetActive(false);
        }
        items[0].SetActive(true);
    }
}
