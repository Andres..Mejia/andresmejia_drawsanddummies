﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckerCollider : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;
    bool mouseOver;
    GameObject currentHit;

    [SerializeField]
    private GameObject particle;

    [SerializeField]
    private GameObject objToActivate;

    void Awake()
    {
        
    }

    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Color startColor = Color.white;

        if (Physics.Raycast(ray, out hit) && Input.GetMouseButton(0))
        {
            currentHit = hit.collider.gameObject;
            if (currentHit.GetComponent<Collider>().tag.Equals("Cube"))
            {
                mouseOver = true;
                if (objToActivate != null)
                {
                    objToActivate.transform.gameObject.SetActive(true);
                }

                DeactivateBlock();
            }
            else
            {
                mouseOver = false;
            }
        }
    }

    void DeactivateBlock()
    {
        //Instantiate(particle, transform.position, transform.rotation);
        currentHit.gameObject.SetActive(false);
        //Destroy(currentHit.gameObject);
    }
}
