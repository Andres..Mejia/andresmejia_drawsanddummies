﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPaintManager : MonoBehaviour
{
        public DrawBehavior drawbehaviorReference;

        public static DrawBehavior drawBehavior;

        void Awake()
        {
            if (drawbehaviorReference == null) Debug.Log("No se ha asignado " + transform.name, gameObject);

            drawBehavior = drawbehaviorReference;

        }
}
