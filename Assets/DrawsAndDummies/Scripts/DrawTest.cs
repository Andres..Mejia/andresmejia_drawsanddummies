﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DrawTest : MonoBehaviour
{
    private bool hideUIWhilePainting = false;
    private bool isUIVisible = true;

    EventSystem eventSystem;

    // Start is called before the first frame update
    void Awake()
    {
        eventSystem = GetComponent<EventSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void MousePaint()
    {
        // mouse is over UI element? then dont paint
        if (eventSystem.IsPointerOverGameObject()) return;
        if (eventSystem.currentSelectedGameObject != null) return;

        // catch first mousedown
        if (Input.GetMouseButtonDown(0))
        {
            if (hideUIWhilePainting && isUIVisible) HideUI();

            // if lock area is used, we need to take full area before painting starts
            if (useLockArea)
            {
                if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, paintLayerMask)) return;
                CreateAreaLockMask((int)(hit.textureCoord.x * texWidth), (int)(hit.textureCoord.y * texHeight));
            }
        }

        // left button is held down, draw
        if (Input.GetMouseButton(0))
        {
            // Only if we hit something, then we continue
            if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, paintLayerMask)) { wentOutside = true; return; }

            pixelUVOld = pixelUV; // take previous value, so can compare them
            pixelUV = hit.textureCoord;
            pixelUV.x *= texWidth;
            pixelUV.y *= texHeight;

            if (wentOutside) { pixelUVOld = pixelUV; wentOutside = false; }


            // lets paint where we hit
            switch (drawMode)
            {
                case DrawMode.Default: // brush
                    DrawCircle((int)pixelUV.x, (int)pixelUV.y);
                    break;

                case DrawMode.Pattern:
                    DrawPatternCircle((int)pixelUV.x, (int)pixelUV.y);
                    break;

                case DrawMode.CustomBrush:
                    DrawCustomBrush((int)pixelUV.x, (int)pixelUV.y);
                    break;

                case DrawMode.FloodFill:
                    if (pixelUVOld == pixelUV) break;
                    CallFloodFill((int)pixelUV.x, (int)pixelUV.y);
                    break;

                case DrawMode.ShapeLines:
                    if (snapLinesToGrid)
                    {
                        DrawShapeLinePreview(SnapToGrid((int)pixelUV.x), SnapToGrid((int)pixelUV.y));
                    }
                    else
                    {

                        DrawShapeLinePreview((int)pixelUV.x, (int)pixelUV.y);
                    }
                    break;

                case DrawMode.Eraser:
                    if (eraserMode == EraserMode.Default)
                    {
                        EraseWithImage((int)pixelUV.x, (int)pixelUV.y);
                    }
                    else
                    {
                        EraseWithBackgroundColor((int)pixelUV.x, (int)pixelUV.y);
                    }
                    break;


                default: // unknown DrawMode
                    Debug.LogError("Unknown drawMode");
                    break;
            }

            textureNeedsUpdate = true;
        }


        if (Input.GetMouseButtonDown(0))
        {
            // take this position as start position
            if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, paintLayerMask)) return;

            pixelUVOld = pixelUV;
        }


        // check distance from previous drawing point and connect them with DrawLine
        //			if (connectBrushStokes && Vector2.Distance(pixelUV,pixelUVOld)>brushSize)
        if (connectBrushStokes && textureNeedsUpdate)
        {
            switch (drawMode)
            {
                case DrawMode.Default: // drawing
                    DrawLine(pixelUVOld, pixelUV);
                    break;

                case DrawMode.CustomBrush:
                    DrawLineWithBrush(pixelUVOld, pixelUV);
                    break;

                case DrawMode.Pattern:
                    DrawLineWithPattern(pixelUVOld, pixelUV);
                    break;

                case DrawMode.Eraser:
                    if (eraserMode == EraserMode.Default)
                    {
                        EraseWithImageLine(pixelUVOld, pixelUV);
                    }
                    else
                    {
                        EraseWithBackgroundColorLine(pixelUVOld, pixelUV);
                    }
                    break;

                default: // other modes
                    break;
            }
            pixelUVOld = pixelUV;
            textureNeedsUpdate = true;
        }

        // left mouse button released
        if (Input.GetMouseButtonUp(0))
        {
            // calculate area size
            if (getAreaSize && useLockArea && useMaskLayerOnly && drawMode != DrawMode.FloodFill)
            {
                LockAreaFillWithThresholdMaskOnlyGetArea(initialX, initialY, true);
            }

            // end shape line here
            if (drawMode == DrawMode.ShapeLines)
            {
                haveStartedLine = false;

                // hide preview line
                lineRenderer.SetPosition(0, Vector3.one * 99999);
                lineRenderer.SetPosition(1, Vector3.one * 99999);
                previewLineCircleStart.position = Vector3.one * 99999;
                previewLineCircleEnd.position = Vector3.one * 99999;

                // draw actual line from start to current pos
                if (snapLinesToGrid)
                {
                    Vector2 extendLine = (pixelUV - new Vector2((float)firstClickX, (float)firstClickY)).normalized * (brushSize * 0.25f);
                    DrawLine(firstClickX - (int)extendLine.x, firstClickY - (int)extendLine.y, SnapToGrid((int)pixelUV.x + (int)extendLine.x), SnapToGrid((int)pixelUV.y + (int)extendLine.y));

                    //DrawLine(firstClickX,firstClickY,SnapToGrid((int)pixelUV.x),SnapToGrid((int)pixelUV.y));
                }
                else
                {

                    // need to extend line to avoid too short start/end
                    Vector2 extendLine = (pixelUV - new Vector2((float)firstClickX, (float)firstClickY)).normalized * (brushSize * 0.25f);
                    DrawLine(firstClickX - (int)extendLine.x, firstClickY - (int)extendLine.y, (int)pixelUV.x + (int)extendLine.x, (int)pixelUV.y + (int)extendLine.y);
                }
                textureNeedsUpdate = true;
            }

            if (hideUIWhilePainting && !isUIVisible) ShowUI(); // show UI since we stopped drawing
        }

    }

    public virtual void HideUI()
    {
        isUIVisible = false;
        userInterface.SetActive(isUIVisible);
    }

    public virtual void ShowUI()
    {
        isUIVisible = true;
        userInterface.SetActive(isUIVisible);
    }
}
