﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleChecker : MonoBehaviour
{
    public GameObject particle;

    void Awake()
    {
        if(gameObject == false)
        {
            ParticleInstance();
        }
    }

    void ParticleInstance()
    {
        Instantiate(particle, transform.position, transform.rotation);
    }
}
