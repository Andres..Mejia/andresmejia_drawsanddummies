﻿public enum DrawMode
{
    Default,
    CustomBrush,
    FloodFill,
    Pattern,
    ShapeLines,
    Eraser
}

public enum EraserMode
{
    Default,
    BackgroundColor
}
